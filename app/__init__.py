from flask import Flask
from flask_session import Session
from dotenv import load_dotenv
import os

# Load environment variables from .env file
load_dotenv()

app = Flask(__name__)

# Configure session to use filesystem
app.config["SESSION_PERMANENT"] = os.getenv("SESSION_PERMANENT")
app.config["SESSION_TYPE"] = os.getenv("SESSION_TYPE")
Session(app)

# Dummy user data from environment variables
users = {
    os.getenv("USER1"): os.getenv("USER1_PASSWORD")
}

products = {
    1: {"name": "Laptop", "price": 1000},
    2: {"name": "Mouse", "price": 50},
    3: {"name": "Keyboard", "price": 100}
}

from app import routes
