# Use a lightweight base image
FROM python:3.9-slim

# Set the working directory in the container
WORKDIR /

# Copy the current directory contents into the container at /app
COPY . /

# Install the required dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Make port 7001 available to the world outside this container
EXPOSE 7001

# Define environment variable
ENV PORT 7001

# Run the application
CMD ["python", "app.py"]
